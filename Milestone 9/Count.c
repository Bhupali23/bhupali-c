#include <stdio.h> 
#include <stdlib.h> 
  
struct List { 
    int data; 
    struct List* next; 
}; 
  
void push(struct List** head_ref, int new_data) 
{ 
    struct List* new_node = (struct List*)malloc(sizeof(struct List)); 
    new_node->data = new_data; 
    new_node->next = (*head_ref); 
    (*head_ref) = new_node; 
} 
  
int count(struct List* head, int search_for) 
{ 
    struct List* current = head; 
    int count = 0; 
    while (current != NULL) { 
        if (current->data == search_for) 
            count++; 
        current = current->next; 
    } 
    return count; 
} 
  
int main() 
{ 
    struct List* head = NULL; 
    push(&head, 5); 
    push(&head, 2); 
    push(&head, 1); 
    push(&head, 3); 
    push(&head, 5);
    push(&head, 5);
  
    printf("No. of times 5 occurs is %d", count(head, 5)); 
    return 0; 
} 
